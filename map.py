#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 16:41:11 2020

@author: anais
@author: celine
"""

################
# Importations #
################

import getopt
import sys

#############
# Functions #
#############

def contains(p: str, bwt: str, n: {}, r: {}) -> (int, int):
    '''
    Allows to know if a pattern p is contained in the reference's suffixes, with no substitutions allowed.
    If the pattern is contained, returns the i numbers of the first and last suffixes in which the pattern is contained.
    Otherwise, returns -1.
    
    Args:
        p : Pattern to match.
        bwt : Burrows Wheeler transform of a DNA string.
        n : Dictionnary returned by the function get_n.
        r : Dictionnary returned by the function get_r.
    '''
    start = 0
    stop = len(bwt)-1
    for pos_pattern in range(len(p)-1,-1,-1):
        current_char = p[pos_pattern]
        new_start = get_down(bwt, current_char, start, stop)
        if new_start == -1: 
            return -1
        else:
            new_stop = get_up(bwt, current_char, start, stop)
            start = left_first(bwt[new_start], r[new_start], n)
            stop = left_first(bwt[new_stop], r[new_stop], n)
    return (start, stop)

def extend(read: str, kmer_pos: int, sa_i: int, max_hamming: int, path_to_ref: str, reverse: bool) -> (int, int):
    '''
    Checks the number if the number of substitions is smaller than max_hamming, and gives the position
    of the first element of the read in the genome and the number of sustitutions. Otherwise, returns -1.
    
    Args:
        read: String of the read of interest.
        kmer_pos: Position of the kmer of interest in the read.
        sa_i: One of the sa_i of the kmer (see get_sa_i).
        max_hamming: Number of maximal substituions allowed.
        path_to_ref: Path of the reference genome.
        reverse: Define if we are working on the reverse read (true), or not (false). 
    '''
    
    read_pos = sa_i - kmer_pos
    char_begin = read_pos
    char_end = read_pos + len(read)
    nb_char = 0
    found = False
    DNA_extract = []
    list_substitutions = []
    
    #Extraction of the lines containing the read in the reference genome
    with open(path_to_ref) as file:
        file.readline() #To skip the header
        for line in file:
            for character in line:
                if (nb_char >= char_begin) and (nb_char < char_end):
                    DNA_extract.append(character)
                elif nb_char >= char_end:
                    found = True
                    break
                nb_char += 1
            if found:
                break
    DNA_extract = ''.join(DNA_extract)
    
    #To prevent an IndexError with the end of the reference genome
    if len(DNA_extract) != len(read):
        return -1
    
    #Comparison between reference and read
    substitutions = 0
    for i in range(len(read)):
        if DNA_extract[i] != read[i]:
            if substitutions < max_hamming:
                substitutions += 1
                list_substitutions.append((read_pos + i, DNA_extract[i], read[i]))
            else:
                return -1
  
    if reverse:
        return (read_pos, '-', substitutions, list_substitutions)
        
    return (read_pos, '+', substitutions, list_substitutions)

def get_bwt_sai_from_index(path_to_file: str) -> (str, [int]):
    '''
    Retrieve the BWT and sa_i with the index file of the reference genome.
    Returns the bwt in a string, and a list of the sa_i.
    
    Args:
        path_to_file : Path to the fasta file of the reference genome.
    '''
    bwt = []
    sa_i = []
    with open(path_to_file) as file:
        file.readline() # To skip the header
        for line in file:
            bwt.append(line.split(" ")[3].rstrip())
            sa_i.append(int(line.split(" ")[1]))
    return (''.join(bwt), sa_i)

def get_down(bwt: str, alpha: chr, start: int, stop: int) -> int:
    '''
    Gets down on the bwt until the alpha character, between the positions start and stop.
    
    Args:
        bwt: Burrows Wheeler transform of a DNA string.
        alpha: Caracter of interest.
        start: Number of the first position.
        stop: Number of the last position.
    '''
    line = start
    while line <= stop:
        if bwt[line] == alpha:
            return line
        line += 1
    return -1

def get_file_lines(path_to_file: str) -> int:
    '''
    Returns the number of lines in a file.
    
    Args:
        path_to_file: Path to the chosen file.
    '''
    nb_lines = 0
    with open(path_to_file) as file:
        for line in file:
            nb_lines += 1
    return nb_lines

def get_n(bwt: str) -> {}:
    '''
    Counts the number of letters in a BWT.
    Results are returned in a dictionnary.
    
    Args:
        bwt: Burrows Wheeler transform of a DNA string.
    '''
    nb_a = 0
    nb_c = 0
    nb_g = 0
    nb_t = 0
    nb_dollar = 0
    for letter in bwt:
        if letter == "A":
            nb_a += 1
        elif letter == "C":
            nb_c += 1
        elif letter == "G":
            nb_g += 1
        elif letter == "T":
            nb_t += 1
        elif letter == "$":
            nb_dollar += 1
    
    results = {"A": nb_a, "C": nb_c, "G": nb_g, "T": nb_t, "$": nb_dollar}
    return results

def get_r(bwt: str) -> {}:
    '''
    Counts the rank for each type of letter. 
    Results are returned in a dictionnary with the position of the letter as the key and the rank as the value.
    
    Args:
        bwt: Burrows Wheeler transform of a DNA string.
    '''
    n = {'$':0, 'A':0, 'C':0, 'G':0, 'T':0}
    r = []
    for letter in bwt:
        n[letter] += 1
        r.append(n[letter])
    return r

def get_up(bwt: str, alpha: chr, start: int, stop: int) -> int:
    '''
    Gets up on the bwt until the alpha character, between the positions start and stop.
    
    Args:
        bwt: Burrows Wheeler transform of a DNA string.
        alpha: Caracter of interest.
        start: Number of the first position.
        stop: Number of the last position.
    '''
    line = stop
    while line >= start:
        if bwt[line] == alpha:
            return line
        line -= 1
    return -1

def kmerisation(read: str, k: int) -> [str]:
    '''
    Gives all the kmers in a list, of a given read.
    
    Args:
        read: Sequence of the read.
        k: Size of the kmers. 
    '''
    kmers = []
    for i in range(len(read)-k+1):
        kmers.append(read[i:i+k])
    return kmers

def left_first(alpha: str, k: int, n: {}) -> int:
    '''
    Gives the line of f (bwt sorted) where the letter alpha number k is.
    
    Args:
        alpha: Letter between A,C,G,T and $.
        k: Rank of the letter alpha in the bwt.
        n: Dictionnary returned by the function get_n.
    '''
    sum_letters = 0
    if alpha == "$":
        return (sum_letters+k-1)
    sum_letters += n["$"]
    if alpha == "A":
        return (sum_letters+k-1)
    sum_letters += n["A"]
    if alpha == "C":
        return (sum_letters+k-1)
    sum_letters += n["C"]
    if alpha == "G":
        return (sum_letters+k-1)
    sum_letters += n["G"]
    if alpha == "T":
        return (sum_letters+k-1)

def reverse_read(read: str) -> str:
    '''
    Gives the reverse read.

    Args:
        read: Read to reverse.
    '''
    read = read[::-1]
    return read.replace('A','X').replace('C','Y').replace('G','C').replace('T','A').replace('X','T').replace('Y','G')
  
########
# Main #
########

if __name__ == "__main__":

    # Getting the arguments
    
    argument_list = sys.argv[1:]
    short_options = "r:i:R:k:mh:ma:o:"
    long_options = ["ref=","index=", "reads=", "max_hamming=", "min_abundance=", "out="]
    path_to_ref = ""
    path_to_index = ""
    path_to_reads = ""
    k = ""
    max_hamming = ""
    min_abundance = ""
    path_to_output = ""
    
    try:
        arguments, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
        print (str(err))
        sys.exit(2)
        
    for current_argument, current_value in arguments:
        if current_argument in ("-r", "--ref"):
            path_to_ref = current_value
        elif current_argument in ("-i", "--index"):
            path_to_index = current_value
        elif current_argument in ("-R", "--reads"):
            path_to_reads = current_value
        elif current_argument in ("-k"):
            k = int(current_value)
        elif current_argument in ("-mh", "--max_hamming"):
            max_hamming = int(current_value)
        elif current_argument in ("-ma", "--min_abundance"):
            min_abundance = int(current_value)
        elif current_argument in ("-o", "--out"):
            path_to_output = current_value
            
    # Creation of a counter 
    
    nb_all_reads = int(get_file_lines(path_to_reads) / 2)
    nb_read = 0
            
    # Mapping of the reads
    
    print("\n### Mapping ###")
    
    list_substitutions = []
    read_name = ""
    read_sequence = ""
    bwt_sai = get_bwt_sai_from_index(path_to_index)
    bwt = bwt_sai[0]
    n = get_n(bwt)
    r = get_r(bwt)
    
    with open(path_to_reads) as read_file:
        for line in read_file:
            if line[0] != ">":
                read_sequence = line.rsplit()[0]
                reverse_sequence = reverse_read(read_sequence)
                reverse = False
                kmer_break = False
                sequence_break = False
                best_position = ()
                
                for sequence in [read_sequence, reverse_sequence]:
                
                    kmers = kmerisation(sequence, k)
                    positions_tested = []
                    
                    #Test each kmer of the read
                    for kmer_index in range(len(kmers)):
                        interval = contains(kmers[kmer_index], bwt, n, r)
                        if interval != -1: #If the kmer is contained in the genome
                            all_sa_i = bwt_sai[1][interval[0] : interval[1]+1]
                            
                            #Test each sa_i of the kmer
                            for sa_i in all_sa_i:
                                this_position = sa_i - kmer_index
                                if this_position not in positions_tested:
                                    positions_tested.append(this_position)
                                    result = extend(sequence, kmer_index, sa_i, max_hamming, path_to_ref, reverse)
                                    if result != -1: #If the number of substitutions is correct
                                        
                                        # Creates an entry in mapping or replace the old entry if the number of substitutions is better
                                        if best_position != ():
                                            if best_position[2] > result[2]:
                                                best_position = result
                                            elif best_position[2] == result[2] and best_position[0] > result[0]:
                                                best_position = result
                                        else:
                                            best_position = result
                                
                                #Breaks the loops if the read got 0 substitutions
                                if best_position != () and best_position[2] == 0:
                                    kmer_break = True
                                    sequence_break = True
                                    break
                            if kmer_break:
                                break
                    if sequence_break:
                        break
                                        
                    reverse = True
                                        
                #Counter
                nb_read += 1
                sys.stdout.write("\r" + str( int(nb_read / nb_all_reads * 100) ) + "% completed")
                
                #Adding substitutions
                if best_position != ():
                    list_substitutions += best_position[3]
                    
    # SNP calling
    print("\n")
    print("### SNP calling ###")
    
    # The list is sorted by the crescent positions
    list_substitutions.sort(key=lambda substitution:substitution[0])
    i = 0
    
    with open(path_to_output, 'w') as output:
        output.write('#REF: ' + path_to_ref + '\n' +
                     '#READS: ' + path_to_reads + '\n' +
                     '#K: ' + str(k) + '\n' +
                     '#MAX_SUBST: ' + str(max_hamming) + '\n' +
                     '#MIN_ABUNDANCE: ' + str(min_abundance) + '\n' +
                     '#POS\tREF\tALT\tABUNDANCE\n')
        
        while i < len(list_substitutions):
            counter = {} # Contains the number of substitutions for each different nucleotide at a position
            position = list_substitutions[i][0] # Reference position
            this_position = position # Position compared to the reference position
            
            # Checks if there is a new position or not
            # If not, counts the number of substitution for each alternative nucleotide
            while position == this_position:
                                
                old_value = counter.get(list_substitutions[i][2], 0)
                counter[list_substitutions[i][2]] =  old_value + 1
                i += 1
                if i >= len(list_substitutions):
                    break
                this_position = list_substitutions[i][0]
                
                #Counter
                sys.stdout.write("\r" + str( int((i+1) / len(list_substitutions) * 100) ) + "% completed")
                
            # Writing of the output                
            for key, value in counter.items():
                if value >= min_abundance:
                    output.write(str(position) + '\t' + list_substitutions[i-1][1] + '\t' + key + '\t' + str(value) + '\n')
            
    print("\n")
                    
                
                