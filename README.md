## Projet ALG 2020
---

This project is a mapper without gaps which detects SNPs.  
This directory contains :  
- A file 'projet_ALG_2020.pdf' with what we were asked to do.
- A directory in which you can put your input files.
- A directory in which you can put your output files.
- A directoy 'tests' in which we have tested the scripts 'map.py' and 'map_bonus.py'.
- A script 'tools_karkkainen_sanders.py' which is used by the script 'index.py', but you do not need to use directly.
- A script 'index.py' that you need to use before the script 'map.py'.
- A script 'map.py' that you will use to map the reads and to produce an output with the SNPs.
- A script 'map_bonus.py' which functions like 'map.py', but without extending all the seeds.

---

#### index.py

This script produces a FM index of the reference genome in a text file, which is necessary for 'map.py'.  
Arguments :  
- --ref (or -r) : Path to the reference genome in a fasta format.
- --out (or -o) : Path of where you want the output to be created.

Example :  
> python3 index.py --ref ./input_data/reference_genome.fa --out ./output_data/index.txt

---

### map.py

This script produces a simplified vcf file, by mapping the reads with a seed and extend process on the reference genome.  
Arguments :  
- --ref (or -r) : Path to the reference genome in a fasta format.
- --index (or -i) : Path to the FM index created with 'index.py'.
- --reads (or -R) : Path to the fasta file which contains the reads.
- -k : Size of the seeds (= k-mers). The shorter they are, the more accurate you are, but it increases the processing time.
- --max_hamming (or -mh) : The maximal number of substitutions (included) allowed while mapping a read.
- --min_abundance (or -ma) : The minimum number of times a substitution must have been sequenced. In other words, the minimal depth coverage of a substitution.
- --out (or -o) : Path of where you want the output to be created.

Example :  
> python3 map.py --ref ./input_data/reference_genome.fa --index ./output_data/index.txt --reads ./input_data/reads.fa -k 20 --max_hamming 5 --min_abundance 3 --out ./output_data/result.vcf

---

### map_bonus.py

This script produces a simplified vcf file, by mapping the reads with a seed and extend process on the reference genome. You can choose the frequency of the extended seeds on all the seeds possible, which can reduce the processing time, but will increase the risk to have false negatives.  
Arguments :
- --ref (or -r) : Path to the reference genome in a fasta format.
- --index (or -i) : Path to the FM index created with 'index.py'.
- --reads (or -R) : Path to the fasta file which contains the reads.
- -k : Size of the seeds (= k-mers). The shorter they are, the more accurate you are, but it increases the processing time.
- --max_hamming (or -mh) : The maximal number of substitutions (included) allowed while mapping a read.
- --min_abundance (or -ma) : The minimum number of times a substitution must have been sequenced. In other words, the minimal depth coverage of a substitution.
- --kmer_frequency (or -K) : The denominator of the seeds frequency. This number is supposed to be superior or equal to 1.
- --out (or -o) : Path of where you want the output to be created.

Example :  
> python3 map.py --ref ./input_data/reference_genome.fa --index ./output_data/index.txt --reads ./input_data/reads.fa -k 20 --max_hamming 5 --min_abundance 3 --kmer_frequency 3 --out ./output_data/result.vcf

---
