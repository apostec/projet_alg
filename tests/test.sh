echo -e "#k\tmax_hamming\tmin_abundance\ttime\tmemory\tprecision\trecall" > test.txt
for ((k=50 ; k>=12 ; k=$k-2))
	do 
	for ((mh=1 ; mh<=25 ; mh=$mh+2)) 
		do 
		for ((ma=1 ; ma<=15 ; ma++)) 
			do 
			/usr/bin/time -f %E,%M python3 ../map.py --ref ../input_data/data_coli/coli/ecoli_sample.fasta --reads ../input_data/data_coli/coli/ecoli_mutated_reads_1000.fasta --index ../output_data/ecoli_index.txt -k $k --max_hamming $mh --min_abundance $ma --out test.vcf 2>time_mem.temp
			
			
			time_mem=$(tail -n 1 time_mem.temp)
			time=$(echo $time_mem | cut -d "," -f 1)
			mem=$(echo $time_mem | cut -d "," -f 2)
			
			validation=$(python3 ../input_data/data_coli/coli/validation.py ../input_data/data_coli/coli/ecoli_mutated_truth.vcf test.vcf)
			
			precision=$(echo $validation | cut -d " " -f 33)
			recall=$(echo $validation | cut -d " " -f 36)
			
			echo -e "$k\t$mh\t$ma\t$time\t$mem\t$precision\t$recall" >> test.txt
			
			done 
		done 
	done 
	
rm time_mem.temp
rm test.vcf
