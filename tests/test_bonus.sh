echo -e "#k\tmax_hamming\tmin_abundance\tkmer_frequency\ttime\tmemory\tprecision\trecall" > test_bonus.txt
for ((k_freq=1 ; k_freq<=25 ; k_freq++))
	do 
	
	/usr/bin/time -f %E,%M python3 ../map_bonus.py --ref ../input_data/data_coli/coli/ecoli_sample.fasta --reads ../input_data/data_coli/coli/ecoli_mutated_reads_1000.fasta --index ../output_data/ecoli_index.txt -k 20 --max_hamming 7 --min_abundance 4 --kmer_frequency $k_freq --out test_bonus.vcf 2>time_mem.temp
	
	time_mem=$(tail -n 1 time_mem.temp)
	time=$(echo $time_mem | cut -d "," -f 1)
	mem=$(echo $time_mem | cut -d "," -f 2)
	
	validation=$(python3 ../input_data/data_coli/coli/validation.py ../input_data/data_coli/coli/ecoli_mutated_truth.vcf test_bonus.vcf)
	
	precision=$(echo $validation | cut -d " " -f 33)
	recall=$(echo $validation | cut -d " " -f 36)
	
	echo -e "20\t7\t4\t$k_freq\t$time\t$mem\t$precision\t$recall" >> test_bonus.txt
			
	done 
	
rm time_mem.temp
rm test_bonus.vcf
