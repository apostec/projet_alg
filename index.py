#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 09:55:23 2020

@author: anais
@author: celine
"""

################
# Importations #
################

import getopt
import sys
import tools_karkkainen_sanders as tks

#############
# Functions #
#############

def get_bwt(s: str, sa: [int]) -> str:
    '''
    Gives the Burrows Wheeler transform of a string s.
    
    Args:
        s: String to transform.
        sa: List of the sorted numbers of the suffixes.
    '''
    bwt = ""
    for i in range(len(s)):
        bwt = bwt + s[(sa[i]-1)%len(s)]
    return bwt

def get_n(bwt: str) -> {}:
    '''
    Counts the number of letters in a BWT.
    Results are returned in a dictionnary.
    
    Args:
        bwt: Burrows Wheeler transform of a DNA string.
    '''
    nb_a = 0
    nb_c = 0
    nb_g = 0
    nb_t = 0
    nb_dollar = 0
    for letter in bwt:
        if letter == "A":
            nb_a += 1
        elif letter == "C":
            nb_c += 1
        elif letter == "G":
            nb_g+=1
        elif letter == "T":
            nb_t+=1
        elif letter == "$":
            nb_dollar += 1
    
    results = {"A": nb_a, "C": nb_c, "G": nb_g, "T": nb_t, "$": nb_dollar}
    return results

def sequence_from_fasta(path_to_file: str) -> str:
    '''
    Get the sequence of a genome in a string from a fasta file.
    
    Args:
        path_to_file: Path to the fasta file of the reference genome.
    '''
    s = ""
    with open(path_to_file) as file:
        file.readline() # To skip the header
        for line in file:
            s += line.rstrip()
    return (s+"$")

########
# Main #
########

if __name__ == "__main__":

    # Getting the arguments
    
    argument_list = sys.argv[1:]
    short_options = "r:o:"
    long_options = ["ref=", "out="]
    path_to_ref = ""
    path_to_output = ""
    
    try:
        arguments, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
        print (str(err))
        sys.exit(2)
        
    for current_argument, current_value in arguments:
        if current_argument in ("-r", "--ref"):
            path_to_ref = current_value
        elif current_argument in ("-o", "--out"):
            path_to_output = current_value
    
    # Creating the output  
        
    sequence = sequence_from_fasta(path_to_ref)
    sa = tks.simple_kark_sort(sequence)
    bwt = get_bwt(sequence, sa)
    number_letters = get_n(bwt)
    f = "$" + number_letters["A"]*"A" + number_letters["C"]*"C" + number_letters["G"]*"G" + number_letters["T"]*"T"
    
    with open(path_to_output, "w") as output:
        output.write("# i sa[i] F L\n")
        for i in range(len(sequence)):
            output.write(str(i) + " " + str(sa[i]) + " " + f[i] + " " + bwt[i] + "\n")
        
